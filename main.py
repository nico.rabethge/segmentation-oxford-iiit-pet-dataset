# Contains sample code from the TorchVision 0.3 Object Detection Finetuning Tutorial
# http://pytorch.org/tutorials/intermediate/torchvision_tutorial.html

import os
import numpy as np
import argparse
import torch
from PIL import Image

import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor

from engine import train_one_epoch, evaluate
from transforms import ToTensor
import utils


class OxfordIIITPetDataset(torch.utils.data.Dataset):
    # https://www.robots.ox.ac.uk/~vgg/data/pets/
    def __init__(self, root):
        self.root = root
        # load all image files, sorting them to
        # ensure that they are aligned
        self.imgs = list(sorted(os.listdir(os.path.join(root, "images"))))
        self.masks = list(sorted(os.listdir(os.path.join(root, f"annotations{os.sep}trimaps"))))
        # somehow there might be a "._" before the filename
        if self.masks[0].startswith("._"):
            self.masks = [fname[2:] for fname in self.masks]

    def __getitem__(self, idx):
        # load images and masks
        img_path = os.path.join(self.root, "images", self.imgs[idx])
        mask_path = os.path.join(self.root, f"annotations{os.sep}trimaps", self.masks[idx])

        img = Image.open(img_path).convert("RGB")

        mask = Image.open(mask_path)
        mask = np.array(mask)

        # instances are encoded as different colors (1: Foreground 2: Background 3: Not classified)
        obj_ids = np.unique(mask)
        # first id is the foreground, so remove the rest
        obj_ids = obj_ids[:1]

        # split the color-encoded mask into a set of binary masks
        masks = mask == obj_ids[:, None, None]

        # get bounding box coordinates for each mask
        num_objs = len(obj_ids)
        boxes = []
        for i in range(num_objs):
            pos = np.where(masks[i])
            xmin = np.min(pos[1])
            xmax = np.max(pos[1])
            ymin = np.min(pos[0])
            ymax = np.max(pos[0])
            boxes.append([xmin, ymin, xmax, ymax])

        # convert everything into a torch.Tensor
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        # there is only one class
        labels = torch.ones((num_objs,), dtype=torch.int64)
        masks = torch.as_tensor(masks, dtype=torch.uint8)

        image_id = torch.tensor([idx])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        # suppose all instances are not crowd
        iscrowd = torch.zeros((num_objs,), dtype=torch.int64)

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["masks"] = masks
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd

        img, target = ToTensor()(img, target)

        return img, target

    def __len__(self):
        return len(self.imgs)


def get_model_instance_segmentation(num_classes):
    # load an instance segmentation model pre-trained pre-trained on COCO
    model = torchvision.models.detection.maskrcnn_resnet50_fpn(pretrained=True)

    # get number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new one
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

    # now get the number of input features for the mask classifier
    in_features_mask = model.roi_heads.mask_predictor.conv5_mask.in_channels
    hidden_layer = 256
    # and replace the mask predictor with a new one
    model.roi_heads.mask_predictor = MaskRCNNPredictor(in_features_mask, hidden_layer, num_classes)

    return model


def main(args: argparse.ArgumentParser):
    # train on the GPU or on the CPU, if a GPU is not available
    device = torch.device(args.torch_device)

    # our dataset has two classes only - background and foreground/pet
    num_classes = 2

    # use our dataset and defined transformations
    dataset = OxfordIIITPetDataset(args.data_root)
    dataset_test = OxfordIIITPetDataset(args.data_root)

    # split the dataset in train and test set
    indices = torch.randperm(len(dataset)).tolist()
    amount_test_samples = int(len(dataset) * args.test_split)
    dataset = torch.utils.data.Subset(dataset, indices[:-amount_test_samples])
    dataset_test = torch.utils.data.Subset(dataset_test, indices[-amount_test_samples:])

    # define training and validation data loaders
    data_loader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True, num_workers=4,
                                              collate_fn=utils.collate_fn)

    data_loader_test = torch.utils.data.DataLoader(dataset_test, batch_size=args.batch_size, shuffle=False,
                                                   num_workers=4, collate_fn=utils.collate_fn)

    # get the model using our helper function
    model = get_model_instance_segmentation(num_classes)

    # move model to the right device
    model.to(device)

    # construct an optimizer
    params = [p for p in model.parameters() if p.requires_grad]

    if args.optimizer == "sgd":
        optimizer = torch.optim.SGD(params, lr=args.lr, momentum=0.9, weight_decay=0.0005)
        lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=3, gamma=0.1)
    elif args.optimizer == "adam":
        optimizer = torch.optim.Adam(params, lr=args.lr)
    else:
        print(f"Optimizer {args.optimizer} isn't available.")

    evaluate(model, data_loader_test, device=device, custom_eval=args.custom_metric, coco=args.coco, log_dir_name=args.vis_folder_name, epoch=0)
    for epoch in range(args.n_epochs):
        # train for one epoch, printing every 10 iterations
        train_one_epoch(model, optimizer, data_loader, device, epoch=epoch+1, print_freq=500)

        if args.optimizer == "sgd":
            # update the learning rate
            lr_scheduler.step()

        # evaluate on the test dataset
        evaluate(model, data_loader_test, device=device, custom_eval=args.custom_metric, coco=args.coco, log_dir_name=args.vis_folder_name, epoch=epoch+1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Oxford IIIT Pet Segmentation')

    parser.add_argument("--torch_device", default="cuda", type=str, help="device on which the tensors will be allocated")
    parser.add_argument("--data_root", default="data", type=str, help="path where the data is located")
    parser.add_argument("--n_epochs", "-e", default=20, type=int, help="amount of training epochs")
    parser.add_argument("--optimizer", default="sgd", type=str, help="optimizer used for training")
    parser.add_argument("--lr", default=0.00001, type=float, help="learning rate")
    parser.add_argument("--batch_size", default=1, type=int, help="batch size")
    parser.add_argument("--test_split", default=0.1, type=float, help="test split")
    parser.add_argument("--coco", default=True, type=bool, help="print coco evaluation metric")
    parser.add_argument("--custom_metric", default=True, type=bool, help="print iou, precision and recall evaluation metric")
    parser.add_argument("--vis_folder_name", default="visualizations", type=str, help="name of the folder for the visualizations")

    args = parser.parse_args()

    main(args)
