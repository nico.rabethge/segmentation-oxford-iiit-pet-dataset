import os
import math
import sys
import time

import torch
import matplotlib.pyplot as plt

import torchvision.models.detection.mask_rcnn
from torchmetrics import IoU, Precision, Recall

from coco_utils import get_coco_api_from_dataset
from coco_eval import CocoEvaluator
import utils


def train_one_epoch(model, optimizer, data_loader, device, epoch, print_freq):
    model.train()
    metric_logger = utils.MetricLogger(delimiter="  ")
    metric_logger.add_meter('lr', utils.SmoothedValue(window_size=1, fmt='{value:.6f}'))
    header = 'Epoch: [{}]'.format(epoch)

    lr_scheduler = None

    if epoch == 0:
        warmup_factor = 1. / 1000
        warmup_iters = min(1000, len(data_loader) - 1)

        lr_scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, start_factor=warmup_factor,
                                                         total_iters=warmup_iters)


    for images, targets in metric_logger.log_every(data_loader, print_freq, header):
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

        loss_dict = model(images, targets)

        losses = sum(loss for loss in loss_dict.values())

        # reduce losses over all GPUs for logging purposes
        loss_dict_reduced = utils.reduce_dict(loss_dict)
        losses_reduced = sum(loss for loss in loss_dict_reduced.values())

        loss_value = losses_reduced.item()

        if not math.isfinite(loss_value):
            print("Loss is {}, stopping training".format(loss_value))
            print(loss_dict_reduced)
            sys.exit(1)

        optimizer.zero_grad()
        losses.backward()
        optimizer.step()

        if lr_scheduler is not None:
            lr_scheduler.step()

        metric_logger.update(loss=losses_reduced, **loss_dict_reduced)
        metric_logger.update(lr=optimizer.param_groups[0]["lr"])

    return metric_logger


def _get_iou_types(model):
    model_without_ddp = model
    if isinstance(model, torch.nn.parallel.DistributedDataParallel):
        model_without_ddp = model.module
    iou_types = ["bbox"]
    if isinstance(model_without_ddp, torchvision.models.detection.MaskRCNN):
        iou_types.append("segm")
    if isinstance(model_without_ddp, torchvision.models.detection.KeypointRCNN):
        iou_types.append("keypoints")
    return iou_types


@torch.no_grad()
def evaluate(model, data_loader, device, log_dir_name: str, epoch: int, custom_eval: bool = True, coco: bool = True, max_n_imgs_visualization: int = 4):
    n_threads = torch.get_num_threads()
    torch.set_num_threads(1)

    if custom_eval:
        iou = IoU(num_classes=2)
        precision = Precision()
        recall = Recall()

        segmentation_ious = []
        segmentation_precisions = []
        segmentation_recalls = []
    if coco:
        coco = get_coco_api_from_dataset(data_loader.dataset)
        iou_types = _get_iou_types(model)
        coco_evaluator = CocoEvaluator(coco, iou_types)

    cpu_device = torch.device("cpu")
    model.eval()
    metric_logger = utils.MetricLogger(delimiter="  ")
    header = 'Test:'

    visualization_finished = False
    for images, targets in metric_logger.log_every(data_loader, 100, header):
        images = list(img.to(device) for img in images)

        if torch.cuda.is_available():
            torch.cuda.synchronize()
        model_time = time.time()
        outputs = model(images)

        outputs = [{k: v.to(cpu_device) for k, v in t.items()} for t in outputs]
        model_time = time.time() - model_time

        if len(images) < max_n_imgs_visualization:
            n_imgs_visualization = len(images)
        else:
            n_imgs_visualization = max_n_imgs_visualization

        plt.figure(figsize=(3, n_imgs_visualization))

        if not visualization_finished:
            for i in range(n_imgs_visualization):
                _ = plt.subplot(n_imgs_visualization, 3, 3 * i + 1)
                current_img = images[i].to(cpu_device).numpy().transpose(1, 2, 0)
                plt.imshow(current_img)
                plt.axis("off")

                _ = plt.subplot(n_imgs_visualization, 3, 3 * i + 2)
                current_target = targets[i]["masks"].to(cpu_device).numpy().transpose(1, 2, 0)
                plt.imshow(current_target)
                plt.axis("off")

                _ = plt.subplot(n_imgs_visualization, 3, 3 * i + 3)
                current_output = outputs[i]["masks"].to(cpu_device).numpy()

                if len(current_output.shape) == 4:
                    current_output = current_output[0]
                current_output = current_output.transpose(1, 2, 0)
                plt.imshow(current_output)
                plt.axis("off")

            plt.savefig(f"logs{os.sep}{log_dir_name}{os.sep}epoch_{epoch}.jpg")
            visualization_finished = True

        if coco:
            res = {target["image_id"].item(): output for target, output in zip(targets, outputs)}
            coco_evaluator.update(res)

        if custom_eval:
            for out, tar in zip(outputs, targets):
                # Only consider the first (, which is the most likely,) mask, because each image contains exactly one animal
                segmentation_ious.append(iou(preds=out["masks"][0], target=tar["masks"]))
                segmentation_precisions.append(precision(preds=out["masks"][0].transpose(0, 2), target=tar["masks"].transpose(0, 2)))
                segmentation_recalls.append(recall(preds=out["masks"][0].transpose(0, 2), target=tar["masks"].transpose(0, 2)))


        evaluator_time = time.time()
        evaluator_time = time.time() - evaluator_time
        metric_logger.update(model_time=model_time, evaluator_time=evaluator_time)

    if custom_eval:
        # with torch.no_grad():
        mIoU = torch.mean(torch.stack(segmentation_ious).detach()).detach()
        mPrecision = torch.mean(torch.stack(segmentation_precisions).detach()).detach()
        mRecall = torch.mean(torch.stack(segmentation_recalls).detach()).detach()

        print(f"mean IoU: {mIoU:.4f}")
        print(f"mean Precision: {mPrecision:.4f}")
        print(f"mean Recall: {mRecall:.4f}")
    if coco:
        # gather the stats from all processes
        metric_logger.synchronize_between_processes()
        print("Averaged stats:", metric_logger)
        coco_evaluator.synchronize_between_processes()

        # accumulate predictions from all images
        coco_evaluator.accumulate()
        coco_evaluator.summarize()

    torch.set_num_threads(n_threads)

    return None
