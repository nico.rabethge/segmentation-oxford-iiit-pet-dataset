# Segmentation Oxford-IIIT Pet Dataset

A segmentation model for the Oxford IIT Pet dataset, which provides the foreground-background segmentation as an output.

## Preparation

Install requirements.txt.

You might encounter an error installing pycocotools using python 3.X.
On Ubuntu installing python3.X-dev solves the problem:

    sudo apt-get install python3.X-dev

## Training

Following meta-parameters can be submitted for the training:  

--n_epochs, default=20, amount of training epochs  
--optimizer, default="sgd", "adam" or "sgd", optimizer used for training  
--lr, default=0.00001, learning rate  
--batch_size, default=1, batch size  
--test_split, default=0.1, test split  

## Evaluation
Before and after the training a visualization of the current segmentation maps/output for a selected amount of images will be generated.  
Furthermore, following metric will be calculated:  

--coco, default=True, whether to print the coco evaluation metric  
--custom_metric, default=True, print iou, precision and recall for the most likely segmentation map as evaluation metric   


